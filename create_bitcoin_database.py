import json

from sqlalchemy import Column, String, Float, Integer
from sqlalchemy import MetaData, Table
from sqlalchemy import create_engine

with open('data/bitcoin_price.json') as f:
    loaded_data = json.load(f)

# print(loaded_data)

# connection = sqlite3.connect('btc_price.db')
# cursor = connection.cursor()
engine = create_engine('sqlite:///btc_usd_price.db')
metadata = MetaData(engine)
btc_usd_table = Table('btc_usd', metadata,
                      Column('symbol', String(10)),
                      Column('time', Integer, nullable=False),
                      Column('open', Float),
                      Column('close', Float),
                      Column('high', Float),
                      Column('low', Float),
                      Column('volume', Float))

metadata.create_all(engine)

# initialize database with btc_usd prices
with engine.connect() as connection:
    # insert records into table
    connection.execute(btc_usd_table.insert(), loaded_data)
    result = connection.execute("select * from `btc_usd`")
    for row in result:
        print(row)
# connection is automatically closed by with statement
