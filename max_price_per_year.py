
from datetime import datetime as dt

from sqlalchemy import MetaData, Table
from sqlalchemy import create_engine
from sqlalchemy import text

engine = create_engine('sqlite:///btc_usd_price.db')

metadata = MetaData(engine)
btc_usd_table = Table('btc_usd', metadata, autoload=True)
# get maximum price for each year, grouped and sorted by year

# turn times to seconds since Unix epoch by / 1000
# and the format the date as a year using '%Y' and STRFTIME sql function
query = """
SELECT MAX(`close`), STRFTIME('%Y', DATE(ROUND(`time` / 1000), 'unixepoch')) as `year`
FROM `btc_usd`
GROUP BY `year`
ORDER BY `year`;
"""

with engine.connect() as connection:
    rows = connection.execute(query)

    for row in rows:
        print(row)
