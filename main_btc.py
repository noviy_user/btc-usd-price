from datetime import datetime as dt

from sqlalchemy import MetaData, Table
from sqlalchemy import create_engine
from sqlalchemy import text

engine = create_engine('sqlite:///btc_usd_price.db')
connection = engine.connect()

metadata = MetaData(engine)
btc_usd_table = Table('btc_usd', metadata, autoload=True)

sql_query = text("SELECT MIN(`time`) as min_time, MAX(`time`) as max_time FROM `btc_usd`")

# return iterator for min_max_dates
rows = connection.execute(sql_query)
min_max_dates = rows.fetchone()  # there will be one row


# fetchall exhaust iterator, so this is not necessary to run
# min_max_dates_as_list = min_max_dates.fetchall() #(no need for that)

# turn times to seconds since Unix epoch by / 1000
print(dt.fromtimestamp(min_max_dates['min_time'] / 1000))
print(dt.fromtimestamp(min_max_dates['max_time'] / 1000))

# close the connection to database
connection.close()
